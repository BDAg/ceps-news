from lxml import html
import requests

print('LEITE ******** LEITE ******** LEITE ******** LEITE ******** LEITE ******** LEITE ******** LEITE ******** LEITE')

tree = html.fromstring(requests.get("https://www.cepea.esalq.usp.br/br/indicador/leite.aspx").content)  # acessa pagina

LeiteValBrutoMed = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[4]/td[4]/text()')    # pega as cotacoes
LeiteValLiqMed = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[4]/td[7]/text()')
LeiteData = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[4]/td[1]/text()')


a = str(LeiteValBrutoMed)
a = a[2:6]  # recorta a parte importante
LeiteValBrutoMed = a

b = str(LeiteValLiqMed)
b = b[2:6]
LeiteValLiqMed = b

aData = str(LeiteData)
aData = aData[2:8]
LeiteData = aData

print('Data ' + LeiteData)
print('Bruto R$ ' + LeiteValBrutoMed)
print('Liquido R$ ' + LeiteValLiqMed)
print()

print('BOI GORDO ******** BOI GORDO ******** BOI GORDO ******** BOI GORDO ******** BOI GORDO ******** BOI GORDO ******** BOI GORDO')

tree = html.fromstring(requests.get("https://www.cepea.esalq.usp.br/br/indicador/boi-gordo.aspx").content)

ValorBoi = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[1]/td[2]/text()')
BoiData = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[1]/td[1]/text()')

c = str(ValorBoi)
c = c[2:8]
ValorBoi = c

bData = str(BoiData)
bData = bData[2:12]
BoiData = bData

print('Data boi ' + BoiData)
print('Valor arroba R$ ' + ValorBoi)
print()

print('FRANGO ******** FRANGO ******** FRANGO ******** FRANGO ******** FRANGO ******** FRANGO ******** FRANGO ******** FRANGO')

tree = html.fromstring(requests.get("https://www.cepea.esalq.usp.br/br/indicador/frango.aspx").content)

ValorFrango = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[1]/td[2]/text()')
FrangoData = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[1]/td[1]/text()')


d = str(ValorFrango)
d = d[2:6]
ValorFrango = d

bData = str(FrangoData)
bData = bData[2:12]
FrangoData = bData

print('Data ' + FrangoData)
print('Valor R$ ' + ValorFrango)
print()

print('OVOS ******** OVOS ******** OVOS ******** OVOS ******** OVOS ******** OVOS ******** OVOS ******** OVOS ******** OVOS')

tree = html.fromstring(requests.get("https://www.cepea.esalq.usp.br/br/indicador/ovos.aspx").content)

ValorOvoBranco = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[1]/td[2]/text()')
ValorOvoVermelho = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[1]/td[4]/text()')
OvoData = tree.xpath('//*[@id="imagenet-indicador1"]/tbody/tr[1]/td[1]/text()')

e = str(ValorOvoBranco)
e = e[2:6]
ValorOvoBranco = e

f = str(ValorOvoVermelho)
f = f[2:6]
ValorOvoVermelho = f

dData = str(OvoData)
dData = dData[2:17]
OvoData = dData

print('Data ovo ' + dData)
print('Valor branco 30 duzias R$ ' + ValorOvoBranco)
print('Valor vermelho 30 duzias R$ ' + ValorOvoVermelho)
print()

